package com.dropit.kotlintask

import java.io.File
import java.net.URL


class Helper {

    // get file from classpath, resources folder
    fun getFileFromResources(fileName: String): File? {
        val classLoader = javaClass.classLoader
        val resource: URL? = classLoader.getResource(fileName)
        return if (resource == null) {
            throw IllegalArgumentException("file is not found!")
        } else {
            File(resource.file)
        }
    }
}
